* DOS-2-DOS UTILITY FUNCTIONS.

        INCLUDE "D2DMAC.ASM"


        XDEF    CMP..,SCAN..,MOVE..,CLIP..,STRIP_LB..,STRIP_TB..
        XDEF    STG_Z..,Z_STG..,STR..,STR2..,LEFT..,RIGHT..
        XDEF    APPEND..,ACHAR..,STRING..,UCASE..,GTLCHAR..
        XDEF    PROMPT..,MOVE_CHARS
;	XDEF	KBD_INPUT,CRLF..,DISP..

        XREF    DosBase,stdin,stdout
;	XREF	INPBUF.,INPBUFF


* COMPARES TWO STRINGS.  SOURCE STRING PTR IN A0, DEST
* PTR IN A1.  RETURNS Z-BIT SET ON STRINGS EQUAL, CLEAR
* IF NOT.

CMP..:
        PUSH    D0
        CLR.L   D0              ;USE FOR LOOP COUNTER
        MOVE.B  (A0)+,D0        ;GET LENGTH OF ONE STRING
        CMP.B   (A1)+,D0        ;SAME?
        BNE     1$              ;NO MATCH
        TST.B   D0              ;ZERO LENGTH?
        BEQ     1$              ;YES...AUTOMATIC MATCH
        SUB.L   #1,D0           ;GRIPES MY ASS
2$:     CMPM.B  (A0)+,(A1)+     ;DO THE COMPARISON
        BNE     1$              ;NO MATCH
        DBF     D0,2$           ;LOOP FOR ALL BYTES
1$:     POP     D0
        RTS


* SCANS A TOKEN FROM THE SOURCE STRING (A0) INTO THE DEST
* STRING (A1).

SCAN..:
        PUSH    A0-A1
        CALL    STRIP_LB..      ;GET RID OF LEADING BLANKS
        POP     A0-A1
        PUSH    D0/D1/A2
        CLR.L   D0
        MOVE.B  (A0),D0         ;LENGTH OF STRING TO SCAN
        MOVE.B  D0,(A1)         ;IN CASE NOTHING LEFT
        BEQ     1$              ;NOTHING TO SCAN
        PUSH    A0              ;SAVE SRC PTR
        ADD.L   #1,A0           ;POINT TO FIRST TEXT BYTE
        MOVE.L  A0,A2           ;SCAN WITH A2, SAVE A0
        MOVE.L  D0,D1           ;SAVE BYTE COUNT
        SUB.L   #1,D0
2$:     CMPI.B  #' ',(A2)       ;BLANK?
        BEQ     3$              ;YES...STOP HERE
        ADD.L   #1,A2           ;POINT TO NEXT TEXT BYTE
        DBF     D0,2$           ;NO...LOOP FOR ALL OF SOURCE
3$:     ADD.L   #1,D0           ;CORRECT FOR FINAL DECREMENT
        SUB.L   D0,D1           ;NUMBER OF BYTES TO MOVE
        MOVE.B  D1,(A1)+        ;LENGTH OF TOKEN
        BEQ     4$              ;NOTHING TO MOVE
        MOVE.L  A0,A2
        SUB.L   #1,D1
5$:     MOVE.B  (A2)+,(A1)+     ;MOVE TOKEN TO DEST
        DBF     D1,5$
4$:     POP     A0
        MOVE.B  D0,(A0)+        ;NEW LENGTH OF SOURCE STRING
        BEQ     1$              ;NOTHING LEFT IN SOURCE STG
        SUB.L   #1,D0
7$:     MOVE.B  (A2)+,(A0)+     ;ELSE MOVE UP REMAINING TEXT
        DBF     D0,7$
1$:     POP     D0/D1/A2
        RTS

* MOVES SOURCE STRING TO DESTINATION STRING.

MOVE..:
        PUSH    D0
        CLR.L   D0
        MOVE.B  (A0),D0         ;LENGTH OF STRING TO MOVE
2$:     MOVE.B  (A0)+,(A1)+     ;MOVE LENGTH AND TEXT
        DBF     D0,2$
1$:     POP     D0
        RTS

* RETURNS THE LAST CHAR OF STRING IN D0.  COMPARES THAT CHAR TO BYTE IN D7.

GTLCHAR..
        ZAP     D0
        MOVE.B  (A0),D0         ;GET CURRENT LENGTH
	MOVE.B	0(A0,D0.W),D0	;GET LAST CHAR
	CMP.B	D0,D7
	RTS

* STRIPS LEADING BLANKS FROM STRING IN A0.

STRIP_LB..:
        PUSH    D0/A1
        MOVE.L  A0,A1           ;SAVE PTR TO SOURCE
        CLR.L   D0
        MOVE.B  (A0)+,D0        ;GET LENGTH
        BEQ     1$              ;NOTHING TO DO
        SUB.L   #1,D0
2$:     CMP.B   #' ',(A0)
        BNE     3$              ;NO BLANK...STOP SCAN
        ADD.L   #1,A0
        DBF     D0,2$           ;SEARCH ENTIRE STRING
3$:     MOVE.B  D0,(A1)         ;STORE BYTE COUNT
        ADD.B   #1,(A1)+        ;ACCOUNT FOR D0 OFFSET
        BEQ.S   1$              ;NOTHING LEFT IN STRING
4$:     MOVE.B  (A0)+,(A1)+     ;MOVE STRING UP
        DBF     D0,4$
1$:     POP     D0/A1
        RTS

*STRIPS TRAILING BLANKS FROM A STRING

STRIP_TB..:
        PUSH    D0/A1
        MOVE.L  A0,A1           ;SAVE PTR TO SOURCE
        CLR.L   D0
        MOVE.B  (A0)+,D0        ;GET LENGTH
        BEQ     1$              ;NOTHING TO SCAN
        ADD.L   D0,A0           ;POINT BEYOND LAST BYTE
        SUB.L   #1,D0
2$:     CMP.B   #' ',-(A0)      ;GOT A BLANK?
        BNE     1$              ;NO...ALL DONE
        SUB.B   #1,(A1)         ;ELSE ONE FEWER BYTES IN STRING
        DBF     D0,2$
1$:     POP     D0/A1
        RTS

* CLIPS D0 LEADING CHARS FROM STRING IN A0.

CLIP..:
        PUSH    D1/A1
        ZAP     D1
        MOVE.L  A0,A1
        MOVE.B  (A0)+,D1        ;GET ORIGINAL LENGTH
        EXT.L   D0
        ADD.L   D0,A0           ;POINT TO REMAINDER OF STRING
        SUB.B   D0,D1           ;CALC NUMBER OF BYTES TO MOVE
        BHI.S   1$              ;OKAY...NON-ZERO BYTE COUNT
        CLR.B   (A1)            ;ELSE NOW HAVE NULL STRING
        BRA.S   2$
1$:     MOVE.B  D1,(A1)+        ;STORE NEW LENGTH
        DECW    D1
3$:     MOVE.B  (A0)+,(A1)+
        DBF     D1,3$
2$:     POP     D1/A1
        RTS

* CONVERTS NULL-TERMINATED STRING IN A0 TO REGULAR STRING IN A1.

Z_STG..:
        PUSH    A0-A2
        MOVE.L  A1,A2
        CLR.B   (A1)+           ;START WITH ZERO LENGTH
1$:     MOVE.B  (A0)+,D0        ;GET A BYTE
        BEQ     2$              ;END OF STRING
	BCLR.B	#7,D0		;GET RID OF POSSIBLE HIGH BIT
        MOVE.B  D0,(A1)+        ;STORE THE CHAR
        INCB    (A2)            ;AND COUNT IT
        BRA.S   1$
2$:     POP     A0-A2
        RTS

* CONVERTS STRING A0 FROM LOWER CASE TO UPPER CASE.

UCASE..:
        PUSH    D0/D1
        CLR.L   D0
        MOVE.B  (A0)+,D0        ;GET LENGTH OF STRING
        BEQ     1$              ;NOTHING TO DO
        SUB.L   #1,D0
3$:     MOVE.B  (A0),D1         ;GET A BYTE
        CMPI.B  #'a',D1
        BLT     2$              ;NOT LOWER CASE ALPHA
        CMPI.B  #'z',D1
        BGT     2$              ;NOT LOWER CASE ALPHA
        ANDI.B  #$5F,D1         ;MAKE IT UPPER
        MOVE.B  D1,(A0)         ;RESTORE CHAR TO STRING
2$:     ADD.L   #1,A0
        DBF     D0,3$           ;LOOP FOR ALL OF STRING
1$:     POP     D0/D1
        RTS

* CONVERTS STRING POINTED TO BY A0 TO NULL-TERMINATED STRING.

STG_Z..:
        PUSH    A0-A1
        ZAP     D0
        MOVE.L  A0,A1           ;POINT A1 TO DEST
        MOVE.B  (A0)+,D0        ;GET LENGTH AND POINT TO 1ST TEXT BYTE
        BEQ     1$              ;NOTHING TO MOVE
        DECW    D0              ;ADJUST FOR DBF
2$:     MOVE.B  (A0)+,(A1)+     ;MOVE UP BY 1 BYTE
        DBF     D0,2$
        CLR.B   (A1)            ;PUT NULL AT END OF STRING
1$:     POP     A0-A1
        RTS

* APPENDS SOURCE STRING (A0) TO END OF DEST STRING (A1), AND ADJUSTS LENGTH
* BYTE.

APPEND..:
        PUSH    A0-A1/D1
        ZAP     D0
        ZAP     D1
        MOVE.B  (A0)+,D0        ;GET LENGTH OF NEW STRING
        BEQ     2$              ;NOTHING TO ADD
        MOVE.B  (A1),D1         ;LENGTH OF ORIGINAL STRING
        ADD.B   D0,(A1)+        ;ADJUST LENGTH OF FINAL STRING
        ADD.L   D1,A1           ;POINT PAST END OF ORIG STG
        DECW    D0              ;CORRECT FOR DBF
1$:     MOVE.B  (A0)+,(A1)+
        DBF     D0,1$
2$:     POP     A0-A1/D1
        RTS

* CONVERTS NUMBER IN D0 TO 2-CHAR ASCII STRING POINTED TO BY A0.  CHAR IN D1
* IS FILL CHAR.

STR2..:
        PUSH    A0/D2-D3
        PUSH    D1              ;SAVE FILLER CHAR
        MOVEQ   #2,D3           ;2-CHAR MAX
        BRA     STRCOM

* CONVERTS THE 26-BIT (MAX) UNSIGNED BINARY NUMBER IN D0 TO 8-CHAR ASCII
* STRING POINTED TO BY A0.  THE DESTINATION MUST BE AT LEAST 8 CHARS LONG.
* CHAR IN D1 IS THE FILL CHAR IF RESULT HAS LEADING 0'S.

STR..:
        PUSH    A0/D2-D3
	PUSH    D1
        MOVEQ   #8,D3           ;MAX 6 DIGITS
STRCOM: MOVE.B  D3,(A0)+        ;STORE AS LENGTH
        ADD.L   D3,A0           ;POINT PAST END OF STRING
        MOVE.L  #10,D2          ;FOR DIVIDE
1$:	PUSH	D0		;SAVE 32-BIT DIVIDEND
	ZAP	D0
	MOVE.W	(SP)+,D0	;GET UPPER 16-BITS
	DIVU    D2,D0           ;DIVIDE NUMBER BY 10
	MOVE.L	D0,D1		;SAVE REMAINDER FOR NEXT DIVISION
	SWAP    D0              ;SAVE QUOTIENT FOR UPPER RESULT
	MOVE.W	(SP)+,D1	;GET LOWER WORD
	DIVU	D2,D1		;DIVIDE UPPER REMAINDER AND LOWER WORD
	MOVE.W	D1,D0		;QUOTIENT TO D0
	SWAP	D1		;REMAINDER
        ADD.B   #$30,D1         ;MAKE INTO ASCII CHAR
        MOVE.B  D1,-(A0)        ;AND STORE INTO BUFFER
        DECW    D3
        TST.L	D0              ;ANY QUOTIENT LEFT?
        BNE.S	1$              ;YES...GO AGAIN
        POP     D0              ;GET FILLER CHAR
        TST.B   D3              ;NO...NEED TO FILL WITH ZERO OR BLANK?
        BEQ.S	3$              ;NO...ALL DONE
        DECW    D3              ;ADJUST FOR DBF
2$:     MOVE.B  D0,-(A0)        ;STORE FILLER CHAR
        DBF     D3,2$
3$:	POP     A0/D2-D3
        RTS

* FILLS STRING (A0) WITH CHAR IN D0, FOR COUNT IN D1.

STRING..:
        MOVE.B  D1,(A0)+        ;STORE LENGTH OF STRING
        DECW    D1
1$:     MOVE.B  D0,(A0)+
        DBF     D1,1$
        RTS


* DSIPLAYS STANDARD STRING (A0) TO SCREEN WITH NO CR-LF.

PROMPT..:
        ZAP     D3
        MOVE.B  (A0)+,D3        ;GET STRING LENGTH IN D3
        BEQ     1$              ;NOTHING...BLANK LINE
        MOVE.L  A0,D2           ;START OF TEXT TO D2
        MOVE.L  stdout,D1       ;FILE HANDLE
	CALLSYS	Write,DosBase
1$:     RTS

* APPENDS A SINGLE CHAR IN D0 TO STRING POINTED TO BY A0.

ACHAR..:
        PUSH    D1
        ZAP     D1
        MOVE.B  (A0),D1         ;GET CURRENT LENGTH
        INCB    (A0)+           ;MAKE STRING 1 LONGER
        ADD.L   D1,A0           ;POINT TO PLACE TO ADD CHAR
        MOVE.B  D0,(A0)         ;PUT NEW CHAR INTO STRING
        POP     D1
        RTS

* EXTRACTS LEFT SUBSTRING (A1) FROM SRC STRING (A0) FOR D0 CHARS OR LESS.

LEFT..:
        PUSH    D1
        MOVE.B  (A0)+,D1        ;GET SOURCE LENGTH
        CMP.B   D1,D0           ;CHECK THAT SOURCE IS LONG ENOUGH
        BCS.S   1$              ;IT IS
        MOVE.B  D1,D0           ;ELSE USE SOURCE LENGTH
1$:     MOVE.B  D0,(A1)+        ;DEST LENGTH
        BEQ.S   2$   
        CALL    MOVE_CHARS
2$:     POP     D1
        RTS

* EXTRACTS RIGHT SUBSTRING (A1) FROM SRC STRING (A0) FOR D0 CHARS OR LESS.

RIGHT..:
        PUSH    D1
        ZAP     D1
        MOVE.B  (A0)+,D1        ;GET SOURCE LENGTH
        CMP.B   D1,D0           ;CHECK THAT SOURCE IS LONG ENOUGH
        BCS.S   1$              ;IT IS
        MOVE.B  D1,D0           ;ELSE USE SOURCE LENGTH
1$:     MOVE.B  D0,(A1)+        ;DEST LENGTH
        BEQ.S   2$
        EXT.L   D0
        ADD.L   D1,A0           ;POINT TO PLACE TO START MOVE
        SUB.L   D0,A0
        CALL    MOVE_CHARS      ;DO IT
2$:     POP     D1
        RTS

* MOVES N TEXT CHARS.  A0=SOURCE, A1=DEST.  D0=MAX BYTE COUNT.

MOVE_CHARS:
	PUSH	D1
        DECW    D0              ;ADJUST FOR DBF
1$:     MOVE.B  (A0)+,D1
	AND.B	#$7F,D1		;GET RID OF HIGH BIT
	MOVE.B	D1,(A1)+	;MOVE A BYTE
        DBF     D0,1$
	POP	D1
        RTS

;* GETS OPERATOR INPUT, WHICH IS STORED IN INPBUF. AS STANDARD STRING.
;
;KBD_INPUT: 
;        READFILE stdin,#INPBUFF,#80
;        SUB.L   #1,D0               ;DON'T COUNT TERMINATOR
;        MOVE.B  D0,INPBUF.
;        LEA     INPBUF.,A0          ;CLEAN UP INPUT STRING
;        CALL    STRIP_LB..
;        LEA     INPBUF.,A0
;        CALL    STRIP_TB..
;        RTS
;
;* DISPLAYS STANDARD STRING (A0) TO SCREEN, FOLLOWED BY CR-LF.
;
;DISP..:
;        CALL    PROMPT..
;        CALL    CRLF..
;        RTS
;
;* SENDS CR-LF TO SCREEN
;
;CRLF..:
;        MOVEQ   #2,D3           ;CR-LF
;        MOVE.L  #CRLF,D2        ;CR-LF STRING
;        MOVE.L  stdout,D1
;	CALLSYS	Write,DosBase
;        RTS
;
;CRLF:   DC.B    13,10

	END


