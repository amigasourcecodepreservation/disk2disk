Dear Disk-2-Disk User,

You know it is wrong to give copies of Disk-2-Disk to other
people.  We want you to think about the effect you have on us
and the entire Amiga market if you do.  There are a few people,
like ourselves, who are struggling to provide the software
tools to make your Amiga more useful and valuable, and, in the
process, help keep the Amiga alive.  When you give away
Disk-2-Disk you hurt us, and ultimately yourself.  

We had planned to put a 'word-from-the-manual' password check
into Disk-2-Disk, but were persuaded that many of you would be
offended by that.  There is no copy protection on Disk-2-Disk.
Some software developers say we are fools.  We prefer to think
we are optimists.  

If you like Disk-2-Disk, tell your friends.  If not, tell us.

Thank you for your support.



Betty and George Chamberlain


Copyright (c) 1987 Central Coast Software.  All rights reserved
worldwide.  This document may not, in whole or in part, be
copied, photocopied, reproduced, translated, or transcribed to
any electronic medium or machine readable form without the
prior written consent of Central Coast Software.

Central Coast Software makes no representation or warranties,
either expressed or implied, with respect to this product, its
quality, performance, merchantability, or fitness for any
particular purpose.  Further, Central Coast Software reserves
the right to revise this product and to make changes from time
to time in the content hereof without obligation of Central
Coast Software to notify any person or organization of such
revisions or changes.

Disk-2-Disk is a trademark of Central Coast Software.  Amiga,
and AmigaDOS are trademarks of Commodore-Amiga, Inc.  Commodore
64 and Commodore 128 are trademarks of Commodore Electronics,
Ltd.

Central Coast Software
268 Bowie Drive
Los Osos, California 93402
(805) 528-4906

April, 1987



TABLE OF CONTENTS




Overview .................................................1

Hardware Requirements ....................................1

Getting Started ..........................................2

Specifying the C64/C128 Disk Drive .......................2

Selecting a file .........................................  2

Copying a file ...........................................  4 

Copying BASIC programs to the Amiga ......................  5

ASCII box ................................................  6

Replace File box .........................................  7

File Type box ............................................  7

Display Text Box .........................................  7

Delete File box ..........................................  7

BASDIF box - Mark BASIC differences ......................  8

Format Disk Box ..........................................  9

Disk Change Box ..........................................  9

BAM Check Box ............................................  9

Read Check Box ...........................................  9

Disk-2-Disk Limitations .................................  10

OVERVIEW


Disk-2-Disk lets the Amiga read and write files on diskettes
from Commodore 64 and Commodore 128 computers.  Disk-2-Disk is
a file transfer utility program which runs on the Commodore
Amiga under AmigaDOS.  Disk-2-Disk transfers all standard
C64/C128 file types to or from 5.25-inch 1541 (single-sided)
or 1571 (double-sided) diskettes.  Disk-2-Disk requires that
your Amiga be equipped with an external 5.25-inch disk drive.

Disk-2-Disk transfers all standard C64/C128 file types to or 
from 5.25-inch 1541 (single-sided) or 1571 (double-sided) 
diskettes.  Disk-2-Disk can read/write 1541 "flippies", but 
the disk manufacturers say that you should not use diskettes 
this way.  Disk-2-Disk cannot read or write CP/M disks or 
Commodore 8050/8250 disks.  

Disk-2-Disk transfers data (binary) files and text (ASCII)
files.  However, Disk-2-Disk does NOT make it possible to run
C64 or C128 programs on the Amiga.  Disk-2-Disk does transfer
C64/C128 program (PRG) files, but they will not run on the
Amiga.  A Commodore BASIC program that can be LISTed on the
C64/C128 can be transferred as a sequential (SEQ) text file to
AmigaDOS, where you can run it as an Amiga BASIC program.  In
most cases you will have to modify the program to run on the
Amiga.  The BASDIF utility included with Disk-2-Disk can help
with these modifications by marking those statements in the
program which must be changed.

Disk-2-Disk has special translation features for document files
produced by Paperclip, Speedscript, or Pocket Writer.  These files
are normally stored as PRG files, and contain special codes which
are unique to the wordprocessing environment.  Disk-2-Disk converts
these files to standard Amiga ASCII, which can be used with almost
any Amiga wordprocessing program.

Disk-2-Disk uses the Intuition interface, windows, and the
mouse.  You can start Disk-2-Disk from the Workbench screen or
from the Command Line Interpreter (CLI).  When you start
Disk-2-Disk, it is loaded into memory from disk and remains
resident until you complete your file transfer session.  This
lets you change AmigaDOS disks without having to maintain a
copy of Disk-2-Disk on each disk.  This is especially helpful
if you have only a single 3.5-inch disk drive on your Amiga.

Disk-2-Disk accepts wild cards in filenames, according to the
wild card conventions of the AmigaDOS operating system.  If the
file to be transferred already exists on the destination disk
under the name you have specified, Disk-2-Disk asks you to
confirm that you wish to replace the old file.   



HARDWARE REQUIREMENTS

Disk-2-Disk requires an Amiga with at least 256KB of memory, an
external 5.25-inch disk drive, and AmigaDOS V1.1 or V1.2.  The
external disk drive can be configured as AmigaDOS drive DF1,
DF2, or DF3.



GETTING STARTED


Kickstart your Amiga in the standard manner with either a V1.1
or V1.2 Kickstart disk.  Insert the Workbench disk as directed.
When the Workbench screen appears, remove the Workbench disk
and insert the Disk-2-Disk disk.  Click on the Disk-2-Disk disk
icon with the left mouse button.  Then click on the Disk-2-Disk
logo icon to start Disk-2-Disk.  Note also the READ ME file,
which you can list to your screen by clicking the READ ME icon.

If you are running from CLI, instead of Workbench, type Disk-2-Disk.

When Disk-2-Disk displays its sign-on greeting, read the
initial message, then click the box in the lower right corner
marked PROCEED.  Disk-2-Disk runs in its own window, which you
can "hide" using the foreground/background gadgets in the upper
right corner.  All of Disk-2-Disk's functions are available in
this one window.  There are no drop-down menus to access.

Note: Disk-2-Disk operates in 80-column mode only.  If you have
selected 60-column mode in Preferences, the Disk-2-Disk filename
boxes may not look right.  To correct the problem, you must
change your Preferences setting to 'Text 80'.

When it first starts, Disk-2-Disk reads and displays the
directory of the C64/C128 disk (if any) in DF2, and also
displays the current AmigaDOS directory.

When you finish using Disk-2-Disk, click on the CLOSE gadget in
the upper left corner of the Disk-2-Disk window to exit from
Disk-2-Disk.



SPECIFYING THE C64/C128 DISK DRIVE


Disk-2-Disk initially assumes that disk drive DF2 is the
5.25-inch drive which will be used for all C64/C128 file
transfers.  If that drive is not correct, click on the drive
code gadget in the C64/C128 filename box above the C64/C128
directory window to change the drive specification.  Then click
on the DISK CHANGE box to force Disk-2-Disk to read the C64/C128
disk in that drive.



SELECTING A FILE


The first step in processing any file is to "select" the
desired file from one of the two directory windows.  Move the
mouse pointer over the the name of the file you wish to
process, and click the left mouse button.  If the desired file
is "off screen", use the scroll gadgets at the right side of
the directory window to move the filename into the window.  You
can select a file from either the C64/C128 directory or from
the AmigaDOS directory.  Files are listed in alphabetic order.

The Amiga directory window initially displays the contents of
the Amiga directory which is current at the time you start
Disk-2-Disk.  To change to another Amiga directory, click on
the PARENT DIR or ROOT DIR boxes, or enter the subdirectory
path into the Amiga filename box.  To change to another Amiga 
device, including RAM:, first type Right-Amiga-X to clear the Amiga
filename box, then enter the device name followed by a colon.
  

The Amiga directory window lists both file names and subdi-
rectory names.  Subdirectory names are highlighted in an
alternate color.  If you click on a subdirectory name,
Disk-2-Disk displays the contents of that subdirectory.  To
return to the previous directory, click on the PARENT DIR box.

Note: C64/C128 disks do not have subdirectories.

When you select a file, Disk-2-Disk copies the selected file
name to the filename box, highlights the name in the filename
box as the "selected" file and sets the direction of the COPY
arrow to reflect the selected file as the source file for the
copy.  The same name is moved into the destination filename
box.  If you want the destination file to have a name different
from that of the source file, you can edit the destination
filename or enter a different name into the destination file-
name box prior to starting the copy.

When you select an AmigaDOS file to be copied to a C64/C128
disk, Disk-2-Disk may not retain the full AmigaDOS filename,
because the AmigaDOS filename may be longer than the maximum of
16 characters permitted in an C64/C128 filename, and may
contain characters which are not legal in C64/C128 filenames.
When this happens, Disk-2-Disk truncates the name to 16
characters, and removes any illegal characters.  You may edit
the name in the destination filename box, if you wish.

Instead of selecting a file using the mouse, you can type the
name of the desired file, complete with path, in the appro-
priate filename box.  If you have not already selected a file,
the filename you enter is highlighted, and Disk-2-Disk sets the
copy direction to show the new name as the source file.



Copying a file


When you are ready to transfer a file, always select the
source file first, as described above.  Move the mouse pointer
to the selected name and press the left mouse button.  Or 
move to the filename box and enter the name of the file you 
wish to transfer.  If you wish to give the file a different 
name on the destination disk, move to the destination filename 
box and enter the desired name.  Otherwise Disk-2-Disk uses the
source file name for the destination name.

Next, examine the direction of the COPY arrow to be certain
that the file will be copied in the direction you want.  The
direction of the arrow indicates the direction of the copy.  To
reverse the direction, click on the "TO" or the "FROM" or
select a file from the opposite directory.  Then, to start the
copy process, click on the COPY "arrow" gadget.  

If you have not selected a file to copy, Disk-2-Disk cannot
perform the copy operation.  

If the selected file name already exists on the destination disk, 
Disk-2-Disk asks if you wish to replace it.  Note that you can 
suppress this message by clicking the "ASK TO REPLACE" box, which 
changes to "AUTO REPLACE".  See page 7.

Disk-2-Disk provides seven forms of transfer: straight binary (no 
conversion), upper case/graphics ASCII, two forms of upper case/lower 
case ASCII, and special conversions for wordprocessing document files 
produced by Paperclip, SpeedScript, and Pocket Writer.  You may have to 
experiment to determine which conversion is best for the file you wish 
to transfer.  You can use the DISPLAY TEXT function to quickly see what 
the conversion would produce, even before you transfer the file.  First 
select the desired file.  Then select the desired conversion in the ASCII 
box before clicking the DISPLAY TEXT box.  

Note: when examining the text on the screen, you can press any key to 
proceed.  However, certain keys, such as the cursor arrow keys, produce 
multiple input characters, which can cause several pages of text to whiz 
by without stopping.  This is not a bug, folks!  Just press a different key!

If the file must be converted from Commodore ASCII to Amiga
ASCII (or vice versa) during the file transfer, you must select
the proper ASCII conversion option before you click on the COPY
arrow.  See page 6.

When Disk-2-Disk copies an Amiga file to a C64/C128 disk, it
must assign a file type (SEQ, REL, USR, or PRG).  The type
assigned is determined by the File Type box.  See page 7.

You can use wild cards in the source file names, if you wish to transfer a 
group of files with a single COPY command.  You must follow the appropriate 
AmigaDOS wildcard usage rules.  For example, to transfer all files on a 
disk, use this form:

                       #?

as the file name.  In this case, the source file name is always used for 
the destination file.

When Disk-2-Disk transfers a group of C64/C128 files to
AmigaDOS, Disk-2-Disk retains the original filename for each
new AmigaDOS file.  When Disk-2-Disk transfers a group of
AmigaDOS files to a C64/C128 disk, the original AmigaDOS
filename is truncated to 16 characters, and illegal characters
are removed.

Disk-2-Disk reads Commodore REL files, but treats them just
like SEQ and USR files.  When you transfer a REL file to or
from a Commodore disk, Disk-2-Disk ignores any "side-sector"
blocks.

We suggest that you enable write-protection on the disk you are
copying from, just to prevent accidents.



COPYING BASIC PROGRAMS TO THE AMIGA


You can use Disk-2-Disk to transfer BASIC programs from the C64
or C128 to the Amiga, where you can convert them to Amiga BASIC
form and run them as Amiga BASIC programs.  But remember:

**************************************************************
* Disk-2-Disk does NOT make it possible to run C64 or C128   *
* programs directly on the Amiga.  Disk-2-Disk does transfer *
* C64/C128 program (PRG) files, but they will NOT run on the *
* Amiga.  						     * 
**************************************************************

C64 and C128 BASIC programs are normally stored using the SAVE
or DSAVE command in a compressed form on a disk as program
(PRG) files.  In such a form, they cannot be used on the Amiga.
To transfer a BASIC program from a C64 or C128 to the Amiga,
you must be able to LIST the program.  Many commercial C64 and
C128 BASIC programs are protected from being listed, and
therefore CANNOT be used on the Amiga.

If a program is written in BASIC, and you can LIST it on the
C64/C128, you can transfer it to the Amiga as a sequential
(SEQ) file in ASCII text format.  First LOAD the file into C64
or C128 memory using the appropriate LOAD command.  Then type
LIST to display the BASIC statements of the file on the screen.
If this works, then type the following commands to LIST the
file to your disk as a SEQ file:

OPEN 1,8,5,"filename,S,W"
CMD 1:LIST

These steps open a sequential file in write mode, and divert
the output of the LIST step to the new file.  If you want to
list only a portion of the file, the LIST command in the second
line can be any of the usual variations of the LIST command.
When the flashing cursor returns, close the file with these
commands:

PRINT#1
CLOSE1

These commands cancel the CMD command and close the new file.
Your file is now stored on the disk as a SEQ file containing
ASCII text in Commodore ASCII format.  Now you can transfer the
program to the Amiga using Disk-2-Disk.  Be certain to specify
the UC/lc ASCII conversion option, described on page 6.  Next,
click the BASDIF box, described on page 8, to flag any
Commodore BASIC statements which must be converted to Amiga
BASIC.  

Finally, start Amiga BASIC and load the new BASIC program, or
click the icon of the new program to run it.  LIST the program
under Amiga BASIC and search for lines containing REM***.  In
most cases you will have to make at least a few changes to the
program before you can expect it to run properly on the Amiga.



ASCII BOX


A text or document file on the C64 or C128 is coded in one of several
formats: upper and lower case characters, upper case only with graphics 
characters, or screen (poke) values.  None of these formats conforms to
the standard format for text storage used by most computers, including 
the Amiga, which is called ASCII.  Therefore, C64/C128 text and document 
files must be converted when they are moved between a C64 or C128 and 
the Amiga. 

The ASCII box selects the type of ASCII text conversion (if any) which 
Disk-2-Disk performs during the file transfer.  Initially, Disk-2-Disk 
assumes NOT ASCII, which means that no text conversion is performed.  In 
this mode, each byte of the file is transferred with no changes.

If you click on this box, it changes to UC/lc 1 ASCII, which specifies 
that the selected file is a text file containing Commodore upper-
case/lowercase characters which are to be converted to Amiga (standard) 
ASCII, or vice versa.

If you click on this box a second time, it changes to UC/lc 2 ASCII, 
which is used when you wish to convert an Amiga ASCII file to a Commodore 
ASCII file with uppercase characters converted to the decimal range of 
97 to 122.  This option is seldom needed.

If you click on this box a third time, it changes to UC/grph ASCII, which 
specifies that the selected file is a text file containing Commodore 
uppercase/graphics characters which are to be be converted to Amiga 
(standard) ASCII, or vice versa.  Note that this form of Commodore ASCII 
has no lowercase characters, so Disk-2-Disk converts all Amiga lowercase 
characters to Commodore uppercase characters when converting from Amiga 
to Commodore.

If you click on the ASCII box again, it changes first to Paperclip, then
SpeedScript, and finally to Pocket Writer.  These selections are for
wordprocessing document (PRG) files.  These documents store their text in 
an even different form.  Disk-2-Disk converts such documents to standard 
Amiga ASCII, which can be processed by most Amiga wordprocessing programs.

Note that Disk-2-Disk cannot detect an incorrect conversion
option, and must do the best it can based on your speci-
fication.  Therefore, you should examine the C64/C128 file (see
DISPLAY TEXT below) before you transfer it to the Amiga to
determine if you have selected the correct ASCII conversion
option.



REPLACE FILE BOX


Click on this box if you wish Disk-2-Disk to replace existing
files without first asking.  Note that the box changes to AUTO
REPLACE.  If you click on the box again, it changes back to ASK
TO REPLACE.



FILE TYPE BOX


When Disk-2-Disk writes a file to a C64/C128 disk, it must
assign a file type (SEQ, REL, USR, or PRG) to the file.  The
purpose of this box is to set the file type for files written
to the C64/C128 disk.  Initially, Disk-2-Disk assumes SEQ.
If you select a C64/C128 file as the source, Disk-2-Disk
automatically changes the File Type box to match the selected
file.  You can also change the file type by clicking on the
box.  Each time you click on the box it changes the file type.



DISPLAY TEXT BOX


The Display Text box displays the text of the selected file in
a special window.  It works with either AmigaDOS or C64/C128
files.  For C64/C128 files (only), Display Text performs the
ASCII conversion specified by the ASCII conversion box as it
displays the text of the file.  This can be very helpful in
determining before you transfer the file which form of ASCII
conversion you should use.

Display Text was designed for use with ASCII text files.  While
you can use it with any file, it works best with text files.
Disk-2-Disk converts tabs to single spaces for the purposes of
display only.



DELETE FILE BOX


Click this box to delete (SCRATCH) the selected AmigaDOS or
C64/C128 file from the specified directory.  The DELETE command
does not accept wildcards.  If you wish to delete a group of
files, you must do so one at a time.



BASDIF BOX - Mark BASIC Differences


Amiga BASIC is very similar to Commodore BASIC, since both were
written by Microsoft.  Commodore BASIC exists in two forms:
BASIC 2.0 on the C64 and BASIC 7.0 on the C128.  Neither of
these dialects exactly matches Amiga BASIC.  Some differences,
such as keyword spacing and line numbers, are more cosmetic
than substantial.  Other differences are more significant.
However, since BASIC is so popular on the C64 and C128,
Disk-2-Disk includes a utility program which examines a text
file and identifies Commodore BASIC statements which are not
compatible with Amiga BASIC.

The BASDIF function applies only to Commodore BASIC files in ASCII 
SEQ form after they they have been transferred to the Amiga.  BASDIF 
cannot process PRG files or any file which is not pure ASCII text.  
Don't expect BASDIF to perform miracles.  It does NOT translate 
Commodore BASIC programs to Amiga BASIC.  It does look for and flag
differences, and cleans up keyword spacing a bit.  The input
file MUST be in ASCII SEQ form. 

If you click on the BASDIF box, Disk-2-Disk asks which version
of BASIC the file represents.  C64 BASIC means BASIC 2.0, and 
C128 BASIC means BASIC 7.0.  Disk-2-Disk then scans the file
specified by the Amiga filename box looking for Commodore BASIC
keywords.  If it finds a keyword which is not valid in Amiga
BASIC, it makes the statement into a remark, by placing the
characters

	REM***

in front of the offending statement.  This even applies to
BASIC statements such as PEEK and POKE, which have identical
syntax in Amiga BASIC, but obviously cannot produce the same
result.  At the same time, Disk-2-Disk inserts spaces between
Commodore BASIC keywords and any operands, as required by Amiga
BASIC.

Disk-2-Disk does not remove line numbers, nor does it delete
any text.  It makes no attempt to translate Commodore BASIC
keywords into equivalent Amiga BASIC keywords, since these
steps depend to a large degree on the logic of the BASIC
program being converted.  However, by marking the differences
between the various versions of BASIC, Disk-2-Disk gives you a
good start toward converting your BASIC programs to run on the
Amiga.

Please note that BASDIF does not perform a full syntax check on
each Commodore BASIC statement.  You may find that Amiga BASIC
detects other problems in the program beyond those flagged by
BASDIF.

BASDIF creates a BASIC program icon for the selected file,
which it stores on the disk along with the marked file.  This
makes it easy to invoke the new BASIC program from Workbench,
as you would any other Amiga BASIC program.



FORMAT DISK BOX - Format 1541 or 1571 diskette


Click on this box to format a 5.25-inch diskette in 1541 or
1571 format.  Disk-2-Disk does not format AmigaDOS disks.  If
you need to format a disk for use with AmigaDOS, you must use
the AmigaDOS format program.

Before formatting the disk, Disk-2-Disk asks you for the name
you wish to assign to the disk, and the type of disk (1541
or 1571).  Note that formatting completely erases all data on
the diskette.

We restricted the Disk-2-Disk FORMAT function to tracks 18-35 
(and 53-70) because we couldn't make formatting work reliably on
tracks 1-17.  This provides a formatted capacity of either 156K 
(1571) or 78K (1541).  If this capacity is not sufficient, you 
can always format a disk on the Commodore machine and bring it 
to your Amiga.  Even restricted to tracks 18 and up, FORMAT is very
slow.  But it works!

Note that when Disk-2-Disk formats a Commodore disk, if does nothing
to tracks 1-17 (and 36-52 if 1571).  This means that if you attempt 
a READ CHECK on your newly-formatted disk, you may find read errors 
on these tracks.  This is normal and should be ignored.




DISK CHANGE BOX


There are two DISK CHANGE boxes: one for the C64/C128 disk, and one
for the Amiga disk.  Click on the appropriate box whenever you change 
a disk.  Disk-2-Disk does not detect such changes automatically.



BAM CHECK BOX


Click on this box if you have reason to suspect the integrity
of the 1541 or 1571 disk you are attempting to use.  Disk-2-Disk
scans the directory of the disk and checks the blocks allocated 
to each file against the Block Availability Map (BAM) of the disk.  
If Disk-2-Disk finds blocks of one file linked into blocks 
allocated to another file, if warns you of the problem on the screen.  
You should not try to copy a corrupted file.



READ CHECK BOX


Click on this box if you wish Disk-2-Disk to perform a read
check of a 1541/1571 disk.  Disk-2-Disk reads every sector of
the disk, to verify the readability of the disk, and stops whenever
it finds a sector which it cannot read.  You must then direct
Disk-2-Disk whether or not to continue the test.  If you experience 
problems reading a diskette, see the discussion of drive alignment 
in the next section.

Note that a READ CHECK on a newly-formatted disk may reveal errors
on tracks 1-17 and 36-52, since Disk-2-Disk does not write anything
to these tracks during the format steps.


DISK-2-DISK LIMITATIONS


The Amiga was not designed to read or write 1541 or 1571 diskettes.  We
have used many "tricks" to force the Amiga hardware to read and write
the unusual format of the Commodore 1541 and 1571 disks.  In addition, the
1541 and 1571 disk drives are notorious for falling out of alignment.  On
top of that, there is great variation in the performace characteristics of
diskettes made by different manufacturers.  

What does all this mean to you?  It means that you may experience 
problems in reading/writing/formatting some diskettes.  We cannot 
guarantee that Disk-2-Disk will work for you in all cases.  Rest 
assured, however, that the program does work properly in the vast 
majority of cases.  

You may experience problems with diskettes formatted or written
on 1541 disk drives.  These disk drives are notoriously prone
to alignment problems.  You may experience difficulties reading
1541 diskettes on your Amiga which work just fine on your 1541
disk drive.  Likewise, you may be unable to read a disk on your
1541 which was formatted and written on the Amiga.  Although
the Amiga Model 1020 5.25-inch disk drive seems to be fairly
tolerant of diskette alignment errors, we cannot guarantee that
it can read all disks.  If you experience this problem, we
suggest that you have your 1541 drive alignment checked at a
competent Commodore repair center.

Some brands of diskettes work better with Disk-2-Disk than others.
If you experience problems with several diskettes of a particular
brand, try using a different brand.  

Disk-2-Disk works with 256-byte sector sizes, and 35-track
5.25-inch diskettes only.  This means that Disk-2-Disk cannot
read disks which are copy-protected using non-standard sector
sizes or other non-standard formatting tricks.

If you experience problems with a disk which is not copy-
protected, try the BAM check or READ check boxes to verify the
integrity of the diskette.  Disk-2-Disk cannot properly trans-
fer files from a disk which is corrupted or was written on a
misaligned 1541 or 1571.

C64 and C128 program (PRG) files do not run on the 68000 micro-
processor of the Amiga.  Likewise, Amiga programs do not run on
the 6510/8510 family of microprocessors of the C64/C128.
Disk-2-Disk transfers program files, but you cannot run the
transferred program on the destination computer.

Disk-2-Disk operates in 80-column mode only.  If you have
selected 60-column mode in Preferences, the filename boxes may
not look right.  To correct the problem, you must change your 
Preferences setting to 'Text 80'.

Some Disk-2-Disk functions are very slow.  We made it as
fast as we could, but forcing the Amiga 5.25-inch disk drive
to read/write the bizarre data format used by the Commodore
disks was very difficult.  Reading the Commodore format is
much easier than writing it, and tracks 18 to 35 are much
easier to process than tracks 1 through 17.  

Writing on tracks 1 to 17 is especially difficult.
Disk-2-Disk allocates disk blocks from the high tracks to the
low tracks, in order to minimize the chance of problems.  We
highly recommend that you transfer only a few files at a time
to a Commodore disk, and don't EVER write onto a Commodore
disk which you can't afford to lose.  Format a new disk and
transfer the files onto it.  Please note: this comment
applies only to WRITING to the Commodore disks.  Reading from
Commodore disks poses no such problems!

Disk-2-Disk locks out interrupts for long periods of time
(sometimes several seconds).  You may observe erratic motion 
of the mouse pointer during file operations to the Commodore 
disk.  This is normal for Disk-2-Disk.  We know of no multi-
tasking problems using Disk-2-Disk, but some other programs 
may not perform properly with interrupts locked out for long 
periods.

Disk-2-Disk estimates Commodore file sizes and free disk
space in units of 254 bytes per block.  The actual file size
is not available in the directory, and would require a
lengthy file search to measure the exact size.  Therefore,
the size of a file on the Amiga side usually does not match
the size of that same file on the Commodore side.  However,
be assured that Disk-2-Disk always transfers exactly the
correct number of bytes, and not more or less.

When you run Disk-2-Disk under AmigaDOS V1.2 with your
5.25-inch disk drive "mounted", Disk-2-Disk tries to stop the
trackdisk driver task for the 5.25-inch drive.  However, if 
the drive has not been used since you booted Workbench, the 
driver task will not be found.  If you access that drive as 
an AmigaDOS device via Disk-2-Disk (or through some other 
program) the driver task may come active and interfere with 
Disk-2-Disk.  To prevent this, you can avoid mounting the 
5.25-inch device, or just don't enter that drive in an Amiga 
pathname.



--------------


Now what we REALLY need for the Amiga is . . .

Is there some program YOU would like to have for your Amiga
which isn't available or costs too much or doesn't offer the
features you need?  If so, let us know.  We are always looking
for ideas for new products, and we would like to hear from you.
Drop us a line or give us a call at (805) 528-4906.

