*********************************************************
*	Copyright (c) 1987 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*********************************************************

	INCLUDE	"D2DMAC.ASM"

	XDEF	ADLBUF,ADNBUF,CDLBUF,CDNBUF
	XDEF	DIRBUF,BAMBUF,BLKBUF,ADBUF	
	XDEF	LINBUF

CDLBUF		DS.B	CDFMAX*4
CDNBUF		DS.B	CDFMAX*28
ADLBUF		DS.B	ADFMAX*4
ADNBUF		DS.B	ADFMAX*38

	CNOP 0,2

LINBUF
DIRBUF		DS.B	DIRSIZ	;DIRECTORY BUFFER
BAMBUF		DS.B	BAMSIZ	;BAM BUFFER
BLKBUF		DS.B	BLKSIZ	;BLOCK BUFFER
ADBUF		DS.B	ADBSIZ	;AMIGA-DOS TEXT BUFFER

	END
